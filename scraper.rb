require 'httparty'
require 'nokogiri'
#require 'rack'
#require 'thin'
#require 'rack/reloader'
print "Enter the URI: "
link = gets.chomp
begin
  html = HTTParty.get("#{URI(link)}")
  response = Nokogiri::HTML(html)
  puts response
rescue TypeError
  puts "Class or module required for rescue clause"
rescue NoMethodError
  puts "Invalid URI! Please include 'https://' before the link or paste the link from the browser."
end
# Rack::Handler::Thin.run HelloWorld.new, :Port => 9292